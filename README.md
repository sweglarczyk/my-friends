# MyFriends REST application #

## Projekt w PHP i Symfony2 z wykorzystaniem REST ##

### ###
### Install: ###

* git clone https://sweglarczyk@bitbucket.org/sweglarczyk/myfriends.git [folder_docelowy]
* cd  [folder_docelowy]
* composer install
* php app/console doctrine:database:create
* php app/console doctrine:schema:create
* php app/console doctrine:fixtures:load

### ###

Dokumentacja + sanbox: 
```
#!php

/api/doc
```


Lista znajomych: 
```
#!php

GET /api/v1/users/{id}/friends.{_format}
```
Dodaj znajomość: 
```
#!php

POST /api/v1/users/{id}/friends/{friendId}.{_format}
```

Usuń znajomość: 
```
#!php

DELETE /api/v1/users/{id}/friends/{friendId}.{_format}
```

Listę znajomych można pobierać paczkami - domyślnie pobiera wszystko (offset=0,limit=null)

```
#!php

GET /api/v1/users/{id}/friends.{_format}?limit={limit}&offset={offset}
```

### ###
{id}, {friendId} - identyfikatory użytkownika, {_format} - możliwy format: json, xml, html