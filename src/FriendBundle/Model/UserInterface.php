<?php

namespace FriendBundle\Model;

interface UserInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set name
     *
     * @param string $name
     * @return UserInterface
     */
    public function setName($name);

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Get friends
     *
     * @param integer $offset
     * @param integer $limit
     * @return array
     */
    public function getFriends($offset = 0, $limit = null);

    /**
     * Add friend
     *
     * @param  UserInterface $user
     * @return void
     */
    public function addFriend($user);

    /**
     * Remove friend
     *
     * @param  UserInterface $user
     * @return void
     */
    public function removeFriend($user);
}