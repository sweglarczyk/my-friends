<?php

namespace FriendBundle\Tests\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase as WebTestCase;


class UserControllerTest extends WebTestCase
{

    public function setUp()
    {
        $this->auth = array(
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW'   => 'password',
        );
        $this->client = static::createClient(array(), $this->auth);
    }

    public function testJsonGetFriendsAction()
    {
        $this->client->request(
            'GET',
            '/api/v1/users/1/friends.json'
        );
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response, 200);
        $content = $response->getContent();

        $decoded = json_decode($content, true);
        $this->assertTrue(isset($decoded[0]) && isset($decoded[0]['id']) && isset($decoded[0]['name']));
    }

    public function testJsonGetFriendActionShouldReturn400WithBadParameters()
    {
        $this->client->request(
            'GET',
            '/api/v1/users/gupek/friends.json'
         );
        $this->assertJsonResponse($this->client->getResponse(), 400, false);
    }

    public function testJsonPostAddFriendActionShouldReturn400WithBadParameters()
    {
        $this->client->request(
            'POST',
            '/api/v1/users/1/friends/1.json'
        );
        $this->assertJsonResponse($this->client->getResponse(), 400, false);
    }

    public function testJsonDeleteFriendAction()
    {
        $this->client->request(
            'GET',
            '/api/v1/users/1/friends.json'
        );
        $response = $this->client->getResponse();
        $this->assertJsonResponse($response, 200);
        $content = $response->getContent();

        $decoded = json_decode($content, true);
        $friendCountBeforeDelete = count($decoded);

        $this->client->request(
            'DELETE',
            sprintf('/api/v1/users/1/friends/%d.json', $decoded[0]['id'])
        );
        $response = $this->client->getResponse();
        $this->assertJsonResponse($this->client->getResponse(), 200);
        $content = $response->getContent();

        $decoded = json_decode($content, true);
        $friendCountAfterDelete = count($decoded);

        $this->assertEquals($friendCountAfterDelete + 1, $friendCountBeforeDelete, true);
    }

    protected function assertJsonResponse($response, $statusCode = 200, $checkValidJson =  true, $contentType = 'application/json')
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', $contentType),
            $response->headers
        );
        if ($checkValidJson) {
            $decode = json_decode($response->getContent());
            $this->assertTrue(($decode != null && $decode != false),
                'is response valid json: [' . $response->getContent() . ']'
            );
        }
    }}
