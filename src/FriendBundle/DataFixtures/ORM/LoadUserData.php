<?php

namespace FriendBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FriendBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userNames = ['Seb','Max','Ewa','Kasia','Zdzich','Pawlo','Teresa','Wojtek','Michalinek','Dorota','Andrzej','Grzesiek','Iza'];
        $friendships = ['Seb'=>['Max','Ewa','Kasia','Zdzich'],'Pawlo'=>['Teresa'],'Wojtek'=>['Michalinek','Dorota'],'Andrzej'=>['Grzesiek','Iza']];

        foreach($userNames as $userName) {
            $user = new User();
            $user->setName($userName);
            $manager->persist($user);
            $manager->flush();
        }

        foreach($friendships as $userName => $friendNames) {
            $user = $manager->getRepository('FriendBundle:User')->findOneByName($userName);

            if ($user instanceof User) {
                foreach($friendNames as $friendName) {
                    $friend = $manager->getRepository('FriendBundle:User')->findOneByName($friendName);
                    if ($friend instanceof User) {
                        $user->addFriend($friend);
                        $manager->persist($user);
                        $manager->flush();
                    }
                }
            }
        }
    }
}