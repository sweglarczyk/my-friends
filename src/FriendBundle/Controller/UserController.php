<?php

namespace FriendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FriendBundle\Model\UserInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends FOSRestController
{

    /**
     * Get single User
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a User for a given id",
     *   output = "FriendBundle\Entity\User",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the user is not found"
     *   }
     * )
     *
     * @Annotations\View(
     *   templateVar = "user"
     * )
     *
     * @param int     $id      the user id
     *
     * @return UserInterface
     *
     * @throws NotFoundHttpException when user does not exist
     */
    public function getUserAction($id)
    {
        $user = $this->getOr404($id);
        return $user;
    }

    /**
     * Get User Friends
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets collection of Friends for User with given id",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when entered id is invalid or param is invalid"
     *   }
     * )
     *
     * @Annotations\View(
     *   templateVar = "user",
     *   serializerEnableMaxDepthChecks = false
     * )
     *
     * @QueryParam(
     *   name = "offset",
     *   requirements = "\d+",
     *   default = "0",
     *   strict = true,
     *   description = "Offset from which to start listing Friends"
     * )
     * @QueryParam(
     *   name = "limit",
     *   requirements = "\d+",
     *   default = null,
     *   nullable = true,
     *   strict = true,
     *   description = "Number of Friends to return"
     * )
     *
     * @param Request               $request      the request object
     * @param int                   $id           the user id
     * @param ParamFetcher $paramFetcher param fetcher service
     *
     * @return array
     *
     * @throws HttpException when entered id is invalid or param is invalid
     */
    public function getUserFriendsAction(Request $request, $id, ParamFetcher $paramFetcher)
    {
        if (!$this->isIdValid($id)) {
            throw new HttpException(400, sprintf('The id \'%s\' is invalid.',$id));
        }

        $offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        $user = $this->getOr404($id);
        return $user->getFriends($offset, $limit);
    }

    /**
     * Create a Friendship relation between users with given ids.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Create a Friendship relation between Users with given ids",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when input data is invalid"
     *   }
     * )
     *
     * @Annotations\View()
     *
     * @param Request $request the request object
     * @param int     $id       the user id
     * @param int     $friendId the friend id
     *
     * @return array
     *
     * @throws HttpException when entered ids are invalid
     */
    public function postUserFriendAction(Request $request, $id, $friendId)
    {
        if (!$this->areFriendsIdValid($id, $friendId)) {
            throw new HttpException(400, sprintf('Entered ids \'%s\', \'%s\' are invalid.',$id, $friendId));
        }
        $user = $this->getOr404($id);
        $friend = $this->getOr404($friendId);
        $user->addFriend($friend);
        $this->save($user);

//        $routeOptions = array(
//            'id' => $user->getId(),
//            '_format' => $request->get('_format')
//        );
//        return $this->routeRedirectView('api_1_get_user_friends', $routeOptions);
        return $user->getFriends();
    }

    /**
     * Delete a Friendship relation between users with given ids.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Delete a Friendship relation between Users with given ids",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when input data is invalid"
     *   }
     * )
     *
     * @Annotations\View()
     *
     * @param Request $request  the request object
     * @param int     $id       the user id
     * @param int     $friendId the friend id
     *
     * @return array
     *
     * @throws HttpException when entered ids are invalid
     */
    public function deleteUserFriendAction(Request $request, $id, $friendId)
    {
        if (!$this->areFriendsIdValid($id, $friendId)) {
            throw new HttpException(400, sprintf('Entered ids \'%s\', \'%s\' are invalid.',$id, $friendId));
        }
        $user = $this->getOr404($id);
        $friend = $this->getOr404($friendId);
        $user->removeFriend($friend);
        $this->save($user);

//        $routeOptions = array(
//            'id' => $user->getId(),
//            '_format' => $request->get('_format')
//        );
//        return $this->routeRedirectView('api_1_get_user_friends', $routeOptions);
        return $user->getFriends();
    }

    /**
     * Fetch a User or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return UserInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($user = $this->container->get('friend.user.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The user with id \'%s\' was not found.',$id));
        }
        return $user;
    }

    /**
     * Save a User.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     */
    protected function save($user)
    {
        $user = $this->container->get('friend.user.handler')->save($user);
        return $user;
    }

    /**
     * Validate single User id.
     *
     * @param mixed $id
     *
     * @return boolean
     */
    protected function isIdValid($id)
    {
        if (!is_numeric($id) || $id <= 0) {
            return false;
        }
        return true;
    }

    /**
     * Validate User id and Friend id.
     *
     * @param mixed $id
     * @param mixed $friendId
     *
     * @return boolean
     */
    protected function areFriendsIdValid($id, $friendId)
    {
        if (!$this->isIdValid($id) || !$this->isIdValid($friendId) || ($id == $friendId)) {
            return false;
        }
        return true;
    }
}
